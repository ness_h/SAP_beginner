jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"opensap/masterdetail/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"opensap/masterdetail/test/integration/pages/App",
	"opensap/masterdetail/test/integration/pages/Browser",
	"opensap/masterdetail/test/integration/pages/Master",
	"opensap/masterdetail/test/integration/pages/Detail",
	"opensap/masterdetail/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "opensap.masterdetail.view."
	});

	sap.ui.require([
		"opensap/masterdetail/test/integration/NavigationJourneyPhone",
		"opensap/masterdetail/test/integration/NotFoundJourneyPhone",
		"opensap/masterdetail/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});