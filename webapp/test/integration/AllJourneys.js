jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 ProductSet in the list

sap.ui.require([
	"sap/ui/test/Opa5",
	"opensap/masterdetail/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"opensap/masterdetail/test/integration/pages/App",
	"opensap/masterdetail/test/integration/pages/Browser",
	"opensap/masterdetail/test/integration/pages/Master",
	"opensap/masterdetail/test/integration/pages/Detail",
	"opensap/masterdetail/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "opensap.masterdetail.view."
	});

	sap.ui.require([
		"opensap/masterdetail/test/integration/MasterJourney",
		"opensap/masterdetail/test/integration/NavigationJourney",
		"opensap/masterdetail/test/integration/NotFoundJourney",
		"opensap/masterdetail/test/integration/BusyJourney",
		"opensap/masterdetail/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});